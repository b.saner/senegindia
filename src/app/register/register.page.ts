import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from "sweetalert2";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild('signupSlider') signupSlider;

  public slideOneForm: FormGroup;
  public slideTwoForm: FormGroup;

  public submitAttempt = false;
  constructor(public formBuilder: FormBuilder) {
    this.slideOneForm = formBuilder.group({
      prenom: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      nom: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      age: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$')])],

    });
    this.slideTwoForm = formBuilder.group({
      telephone: ['', Validators.required],
      password: ['',  Validators.required],
      adresse: ['', Validators.compose([Validators.required, Validators.minLength(4)])],

    });

  }

  ngOnInit() {

  }
  next(){
    this.signupSlider.slideNext();
  }

  prev(){
    this.signupSlider.slidePrev();
  }

  save(){
    this.submitAttempt = true;

    if(!this.slideOneForm.valid){
      this.signupSlider.slideTo(0);
    }
    else if(!this.slideTwoForm.valid){
      this.signupSlider.slideTo(1);
    }
    else {
      console.log("success!")
      Swal.fire({
        icon: 'success',
        // title: 'Merci de votre inscription',
        html: '<p style="font-size: 16px">Merci de votre inscription</p>',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,

      });
      console.log(this.slideOneForm.value);
      console.log(this.slideTwoForm.value);
    }
  }
}
