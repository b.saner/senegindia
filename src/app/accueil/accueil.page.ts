import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {
  public registerForm: FormGroup;
  slideOpts = {
    initialSlide: 1,
    speed: 500,
    autoplay: {
      delay: 3500,
      disableOnInteraction: false,
    },
    pagination: {
      dynamicBullets: false,
    },
  };
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email ]]} );
  }

  onSubmit() {
      Swal.fire({
          icon: 'success',
          // title: 'Merci de votre inscription',
          html: '<p style="font-size: 16px">Merci de votre inscription</p>',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,

      });
 // Swal.fire({
 //                  title: 'success', icon: 'success', html: '<p style="font-size: 12px">Merci de votre inscription</p>',
 //                  showConfirmButton: false, timer: 2500, timerProgressBar: true,
 //              });
  }
    get f() {
        return this.registerForm.controls;
    }
}
