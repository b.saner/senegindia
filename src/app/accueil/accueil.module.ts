import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccueilPageRoutingModule } from './accueil-routing.module';

import { AccueilPage } from './accueil.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AccueilPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [AccueilPage]
})
export class AccueilPageModule {}
