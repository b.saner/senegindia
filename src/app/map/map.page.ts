import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit , AfterViewInit{
  map: Map;
  newMarker: any;


  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.map = new Map('mapId');
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        //   {
        //     attribution:
        //         'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors,
        //         <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
        //   }
    ).addTo(this.map);
    // }
    this.locatePosition();
  }
  locatePosition() {
    this.map.locate({ setView: true, maxZoom: 12 , enableHighAccuracy : true}).on('locationfound', (e: any) => {
      console.log('location', e);
      // Si la localisation es disponible on integrera notre position
      this.newMarker = marker([e.latitude, e.longitude], {
        draggable: false
      }).addTo(this.map);
      this.newMarker.bindPopup('<p>Ma position</p>');
    });

  }
}
