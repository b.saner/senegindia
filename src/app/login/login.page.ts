import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public registerForm: FormGroup;
  showpassword = false;
  public submitted = false;

  passwordToggle = 'eye';

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  togglepassword(): void {
    this.showpassword = !this.showpassword;
    if (this.passwordToggle === 'eye')  {
      this.passwordToggle = 'eye-off';
    } else {
      this.passwordToggle = 'eye';
    }
  }
  get f() {
    return this.registerForm.controls;
  }

  public async onSubmit() {

  }
}
